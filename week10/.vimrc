"source function.vim

set number
set ignorecase
set autoindent
set smartindent
set title
set tabstop=4

set backup

nnoremap <F2> :w
nnoremap <F3> :wq
nnoremap <F6> :q! 


"function! ToggleNumber()
"	if(&relativenumber == 1)
"		set norelativenumber
"		set number
"	else
"		set relativenumber
"	endif
"endfunc
"command! ToHtml :so $VIMRUNTIME/syntax/2html.vim
"command! Ncd :cd %:p:h

