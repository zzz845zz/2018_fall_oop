#include "animal.h"
#include "Function.h"
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <vector>

using namespace std;

Fish::Fish() {
    setFreshwater(false);
    setHabitat("");
    setPredator(false);
}
void Fish::readInfo() {
    vector<string> str[2];
	vector<string>::iterator iter[2];
	string line, filename = "Fish.csv";
	ifstream inFile;
	inFile.open(filename);

	if (inFile.fail()) {
		cout << "File does not exist." << endl;
		return;
	}

	for (int i = 0;i < 2;i++) {
		getline(inFile, line);
		stringstream sep(line);
		while (!sep.eof()) {
			getline(sep, line, ',');
			str[i].push_back(line);
		}
	}

	iter[0] = str[0].begin();
	iter[1] = str[1].begin();

	for (int i = 0;iter[0] + i < str[0].end();i++) {
		if (*(iter[0] + i) == "name") {
			Fish::setName(*(iter[1] + i));
		}
		else if (*(iter[0] + i) == "color") {
			Fish::setColor(*(iter[1] + i));
		}
		else if (*(iter[0] + i) == "freshwater?") {
            if(*(iter[1]+i) == "TRUE")
                Fish::setFreshwater(true);
            else if(*(iter[1]+i) == "FALSE")
                Fish::setFreshwater(false);
		}
		else if (*(iter[0] + i) == "habitat") {
			Fish::setHabitat(*(iter[1] + i));
		}
		else if (*(iter[0] + i) == "predator?") {
            if(*(iter[1]+i) == "TRUE")
                Fish::setPredator(true);
            else if(*(iter[1]+i) == "FALSE")
                Fish::setPredator(false);
		}
	}

	inFile.close();

	try{
        bool check = Fish::getFreshwater();
		if (check != true && check!= false)
			throw 0;
	}
	catch(int expn){
        if(expn==0)
			Fish::setFreshwater(checkBool("Fish", "FreshWater"));
	}
};
void Fish::printInfo() {
    cout << "Fish Information:" << endl;
    cout << "Name: " << Fish::getName() << endl;
    cout << "Color: "<< Fish::getColor() << endl;
    cout << "FreshWater: "<<Fish::convertBool(getFreshwater()) << endl;
    cout << "Habitat: "<<Fish::getHabitat() << endl;
    cout << "Predator: "<<Fish::convertBool(getPredator())<<endl;
};
string Fish::convertBool(bool _value) {
    if(_value) 
        return "TRUE";
    else return "FALSE";
};