#include "animal.h"
#include "Function.h"
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <vector>

using namespace std;
Monkey::Monkey() {
    setWild(false);
    setHome("");
    setEndangered(false);
    setAge(0);
};

void Monkey::readInfo() {
	vector<string> str[2];
	vector<string>::iterator iter[2];
	string line, filename = "Monkey.csv";
	ifstream inFile;
	inFile.open(filename);

	if (inFile.fail()) {
		cout << "File does not exist." << endl;
		return;
	}

	for (int i = 0;i < 2;i++) {
		getline(inFile, line);
		stringstream sep(line);
		while (!sep.eof()) {
			getline(sep, line, ',');
			str[i].push_back(line);
		}
	}

	iter[0] = str[0].begin();
	iter[1] = str[1].begin();

	for (int i = 0;iter[0] + i < str[0].end();i++) {
		if (*(iter[0] + i) == "name") {
			Monkey::setName(*(iter[1] + i));
		}
		else if (*(iter[0] + i) == "color") {
			Monkey::setColor(*(iter[1] + i));
		}
		else if (*(iter[0] + i) == "age") {
			Monkey::setAge(stoi(*(iter[1] + i)));
		}
		else if (*(iter[0] + i) == "wild?") {
			if(*(iter[1] + i) == "TRUE")
				Monkey::setWild(true);
			else if(*(iter[1] + i) == "FALSE")
				Monkey::setWild(false);
		}
		else if (*(iter[0] + i) == "home") {
			Monkey::setHome(*(iter[1] + i));
		}
		else if (*(iter[0] + i) == "endagered") {
			if(*(iter[1] + i) == "TRUE")
				Monkey::setEndangered(true);
			else if(*(iter[1] + i) == "FALSE")
				Monkey::setEndangered(false);
		}
	}

	inFile.close();

	try{
        bool checkWild = Monkey::getWild();
		bool checkEndanger = Monkey::getEndangered();
		if (checkWild != true && checkWild != false)
			throw 0;
		if (checkEndanger != true && checkEndanger != false)
			throw 1;
		if (Monkey::getAge() == 0)
			throw 2;
	}
	catch(int expn){
		switch(expn){
			case 0:
				Monkey::setWild(checkBool("Monkey", "Wild"));
				break;
			case 1:
				Monkey::setEndangered(checkBool("Monkey", "Endangered"));
				break;
			case 2:
				Monkey::setAge(checkAge("Monkey"));
		}
	}    
};
void Monkey::printInfo() {
    cout << "Monkey Information:" << endl;
    cout << "Name: " << Monkey::getName() << endl;
    cout << "Color: "<< Monkey::getColor() << endl;
    cout << "Age: "<<Monkey::getAge() << endl;
    cout << "Wild: "<<Monkey::convertBool(Monkey::getWild()) << endl;
    cout << "Home: "<<Monkey::getHome() <<endl;
	cout << "Endangered: "<<Monkey::convertBool(Monkey::getEndangered()) << endl; 
};
string Monkey::convertBool(bool _value) {
	if(_value) return "TRUE";
	else return "FALSE";
};
void Monkey::changeEndangered() {
	Monkey::setEndangered(!Monkey::getEndangered());
};