#include "animal.h"
#include "Function.h"
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <vector>

using namespace std;
    Dog::Dog() {
        Dog::setBreed("");
        Dog::setWeight(0);
        Dog::setAge(0);
    }
    
    void Dog::readInfo() {
        vector<string> str[2];
	vector<string>::iterator iter[2];
	string line, filename = "Dog.csv";
	ifstream inFile;
	inFile.open(filename);

	if (inFile.fail()) {
		cout << "File does not exist." << endl;
		return;
	}

	for (int i = 0;i < 2;i++) {
		getline(inFile, line);
		stringstream sep(line);
		while (!sep.eof()) {
			getline(sep, line, ',');
			str[i].push_back(line);
		}
	}

	iter[0] = str[0].begin();
	iter[1] = str[1].begin();

	for (int i = 0;iter[0] + i < str[0].end();i++) {
		if (*(iter[0] + i) == "Name") {
			Dog::setName(*(iter[1] + i));
		}
		else if (*(iter[0] + i) == "Breed") {
			Dog::setBreed(*(iter[1] + i));
		}
		else if (*(iter[0] + i) == "Age") {
			Dog::setAge(stoi(*(iter[1] + i)));
		}
		else if (*(iter[0] + i) == "Color") {
			Dog::setColor(*(iter[1] + i));
		}
		else if (*(iter[0] + i) == "Weight") {
			Dog::setWeight(stoi(*(iter[1] + i)));
		}
	}

	inFile.close();

	try{
		if (Dog::getAge() == -1)
			throw 0;

		if (Dog::getWeight() == -1)
			throw 1;
	}
	catch(int expn){
		switch(expn){
			case 0:
				Dog::setAge(checkAge("Dog"));
			 	break;
			case 1:
				Dog::setWeight(checkWeight(filename));
				break;
		}
	}
};
    void Dog::printInfo() {
        cout << "Dog Information:" << endl;
        cout << "Name: " << Dog::getName() << endl;
        cout << "Breed: "<< Dog::getBreed() << endl;
        cout << "Age: "<<Dog::getAge() <<" (years)"<< endl;
        cout << "Color: "<<Dog::getColor() << endl;
        cout << "Weight: "<<Dog::getWeight()<<" (pounds)"<<endl;
    };
    void Dog::subtractTen() {
        Dog::weight -=  10;
    };