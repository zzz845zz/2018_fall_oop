#include "animal.h"
#include "Function.h"
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <vector>

    string habitat;
    bool isProtected;
    int weight;

Lizard::Lizard() {
    Lizard::setHabitat("");
    Lizard::setProtected(false);
    Lizard::setWeight(0);
}

void Lizard::readInfo() {
	vector<string> str[2];
	vector<string>::iterator iter[2];
	string line, filename = "Lizard.csv";
	ifstream inFile;
	inFile.open(filename);

	if (inFile.fail()) {
		cout << "File does not exist." << endl;
		return;
	}

	for (int i = 0;i < 2;i++) {
		getline(inFile, line);
		stringstream sep(line);
		while (!sep.eof()) {
			getline(sep, line, ',');
			str[i].push_back(line);
		}
	}

	iter[0] = str[0].begin();
	iter[1] = str[1].begin();

	for (int i = 0;iter[0] + i < str[0].end();i++) {
		if (*(iter[0] + i) == "name") {
			Lizard::setName(*(iter[1] + i));
		}
		else if (*(iter[0] + i) == "color") {
			Lizard::setColor(*(iter[1] + i));
		}
		else if (*(iter[0] + i) == "habitat") {
			Lizard::setHabitat(*(iter[1] + i));
		}
		else if (*(iter[0] + i) == "protected?") {
			if(*(iter[1] + i) == "TRUE") 
                Lizard::setProtected(true);
            else if(*(iter[1] + i) == "FALSE") 
				Lizard::setProtected(false);
		}
		else if (*(iter[0] + i) == "weight") {
			Lizard::setWeight(stoi(*(iter[1] + i)));
		}
	}

	inFile.close();

	try{
        bool check = Lizard::getProtected();
		if (check != true && check != false)
			throw 0;

		if (Lizard::getWeight() == 0)
			throw 1;
	}
	catch(int expn){
		switch(expn){
			case 0:
				Lizard::setProtected(checkBool("Lizard", "Protected"));
				break;
			case 1:
				Lizard::setWeight(checkWeight("Lizard"));
		}
	}
};
void Lizard::printInfo() {
    cout << "Lizard Information:" << endl;
    cout << "Name: " << Lizard::getName() << endl;
    cout << "Color: "<< Lizard::getColor() << endl;
    cout << "Habitat: "<<Lizard::getHabitat() << endl;
    cout << "Protected: "<<Lizard::convertBool(Lizard::getProtected()) << endl;
    cout << "Weight: "<<Lizard::getWeight()<<" (ounces)"<<endl;
};
string Lizard::convertBool(bool _value) {
    if(_value) return "TRUE";
    else return "FALSE";
}