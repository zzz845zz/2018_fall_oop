#include "animal.h"
#include "Function.h"
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <vector>

using namespace std;

Horse::Horse() {
    Horse::setManeColor("");
    Horse::setHeight(0);
    Horse::setAge(0);
}

void Horse::readInfo() {
    vector<string> str[2];
	vector<string>::iterator iter[2];
	string line, filename = "Horse.csv";
	ifstream inFile;
	inFile.open(filename);

	if (inFile.fail()) {
		cout << "File does not exist." << endl;
		return;
	}

	for (int i = 0;i < 2;i++) {
		getline(inFile, line);
		stringstream sep(line);
		while (!sep.eof()) {
			getline(sep, line, ',');
			str[i].push_back(line);
		}
	}

	iter[0] = str[0].begin();
	iter[1] = str[1].begin();

	for (int i = 0;iter[0] + i < str[0].end();i++) {
		if (*(iter[0] + i) == "name") {
			Horse::setName(*(iter[1] + i));
		}
		else if (*(iter[0] + i) == "maneColor") {
			Horse::setManeColor(*(iter[1] + i));
		}
		else if (*(iter[0] + i) == "age") {
			Horse::setAge(stoi(*(iter[1] + i)));
		}
		else if (*(iter[0] + i) == "color") {
			Animal::setColor(*(iter[1] + i));
		}
		else if (*(iter[0] + i) == "height") {
			Horse::setHeight(stoi(*(iter[1] + i)));
		}
	}

	inFile.close();

	try{
		if (Horse::getAge() == 0)
			throw 0;

		if (Horse::getHeight() == 0)
			throw 1;
	}
	catch(int expn){
		switch(expn){
			case 0:
				Horse::setAge(checkAge("Horse"));
				break;
			case 1:
				Horse::setHeight(checkHeight("Horse"));
		}
	}
};
void Horse::printInfo() {
    cout << "Horse Information:" << endl;
    cout << "Name: " << Horse::getName() << endl;
    cout << "Color: "<< Horse::getColor() << endl;
    cout << "Mane Color: "<<Horse::getManeColor() << endl;
    cout << "Age: "<<Horse::getAge() << " (years)" << endl;
    cout << "Height: "<<Horse::getHeight()<<" (hands)"<<endl;
};