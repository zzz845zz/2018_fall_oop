#include "Icecream.h"
#include <string>
#include <iostream>

using namespace std;

IceCream::IceCream()
{}
IceCream::IceCream(bool type, int numberOfScoops, string dessertName)
{
    setType(type);
    setNumberOfScoops(numberOfScoops);
    setName(dessertName);
}

void IceCream::setNumberOfScoops(int number) 
{
    numberOfScoops = number;
}
int IceCream::getNumberOfScoops()
{
    return numberOfScoops;
}

void IceCream::setType(bool type) 
{
    isCone = type;
}
bool IceCream::getType()
{
    return isCone;
}

double IceCream::getCost()
{
    return isCone? getNumberOfScoops()+0.5:getNumberOfScoops();
}

void IceCream::print()
{
        cout << getName() << endl;
        cout << getNumberOfScoops() << " scoops ";
        if(isCone) cout << "+ cone";
        cout << endl;
}
