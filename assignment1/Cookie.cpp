#include "Cookie.h"
#include <string>
#include <iostream>

using namespace std;

Cookie::Cookie(int numberOfCookies, double pricePerDozen, string dessertName)
{
	setNumberOfCookies(numberOfCookies);
	setPricePerDozen(pricePerDozen);
	setName(dessertName);
}

void Cookie::setNumberOfCookies(int number) 
{
	numberOfCookies = number;
}
double Cookie::getNumberOfCookies()
{
	return numberOfCookies;
}

void Cookie::setPricePerDozen(double price)
{
	pricePerDozen = price;
}
double Cookie::getPricePerDozen()
{
	return pricePerDozen;
}

double Cookie::getCost()
{
	return (getNumberOfCookies()/12)*getPricePerDozen();
}


void Cookie::print()
{
        cout << getName() << endl;
        cout << getNumberOfCookies() << "cookies @"<< getPricePerDozen() << " per dozen"<< endl;
};
