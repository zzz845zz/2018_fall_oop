#include "Icecream.h"
#include "Sundae.h"
#include <string>
#include <iostream>

using namespace std;

Sundae::Sundae(double costOfTopping, bool type,
                int numberOfScoops, string dessertName)
{
    setCostOfTopping(costOfTopping);
    setType(type);
    setNumberOfScoops(numberOfScoops);
    setName(dessertName);
}

void Sundae::setCostOfTopping(double cost)
{
    costOfTopping = cost;
}
double Sundae::getCostOfTopping()
{
    return costOfTopping;
}

double Sundae::getCost()
{
    double totalCost 
        = getNumberOfScoops()+getCostOfTopping();
    if(getType()) return totalCost+0.5;
    else return totalCost;
}

void Sundae::print()
{
        cout << getName() << endl;
        cout << getNumberOfScoops() << "scoops ";
        if(getType()) cout << "+ cone" << endl;
        cout << "+ " << getCostOfTopping() <<" toppings" << endl;
}
