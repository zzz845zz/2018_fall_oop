#include "TrianglePattern.h"
#include <iostream>
#include <string>

using namespace std;

TrianglePattern::TrianglePattern() 
{
    set_height(0);
    set_pattern('*');
}
TrianglePattern::TrianglePattern(int x)
{
    set_height(x);
    set_pattern('*');
}

void TrianglePattern::set_height(int x) {
    height = x;
}
int TrianglePattern::patternHelper(int h) {
    if(h>=0) return 1;
    else return 0;
}

void TrianglePattern::printPattern() {
    
    if(patternHelper(height)) {
        cout << "The Right Triangle Pattern : (height = "
        << height << " )"<< endl;

        for(int y=height; y>=1; y--) {
            for(int x=0; x<y; x++) {
                cout << get_pattern();
            }
            cout << endl;
        }
        cout << endl;
    }
    else {
        cout<<"Invalid size!\n"<<endl;
    }
}