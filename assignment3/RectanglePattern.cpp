#include "RectanglePattern.h"
#include <iostream>
#include <string>

using namespace std;

RectanglePattern::RectanglePattern() 
{
    set_lenghth(0);
    set_width(0);
    set_pattern('*');
}
RectanglePattern::RectanglePattern(int x, int y)
{
    set_lenghth(x);
    set_width(y);
    set_pattern('*');
}

void RectanglePattern::set_lenghth(int x) {
    length = x;
}
void RectanglePattern::set_width(int y) {
    width = y;
}
int RectanglePattern::patternHelper(int l, int w) {
    if(l>=0 && w>>0) return 1;
    else return 0;
}

void RectanglePattern::printPattern() {
    if(patternHelper(length, width)) {
        cout << "The Rectangle Pattern : (length = "
        << length << ", width = " << width <<" )"<< endl;

        for(int y=0; y<width; y++) {
            for(int x=0; x<length; x++) {
                cout << get_pattern();
            }
            cout << endl;
        }
        cout << endl;
    }
    else {
        cout << "Invalid size!\n" << endl;
    }
}