#include <iostream>

using namespace std;

int main() {
    int score;

    cin >> score;
    if(score>=100) {
        cout << "your grade is A" << endl;
    }
    else if(score>=90) {
         cout << "your grade is B" << endl;
    }
    else if(score>=80) {
         cout << "your grade is C" << endl;
    }
    else if(score>=70) {
         cout << "your grade is D" << endl;
    }
    else {
         cout << "your grade is F" << endl;
    }

    return 0;
}