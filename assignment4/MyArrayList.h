#ifndef MYARRAYLIST_H
#define MYARRAYLIST_H

#include <iostream>
#include <string>
using namespace std;

template <typename T>
class MyArrayList {
    public:
        MyArrayList();
        MyArrayList(int x);
        bool addElement(T const &x);
        void printIt();

    private:
        const static int DEFAULT_CAPACITY=100;
        T * mass;
        int capacity=0;
        int size=0;
};
#endif