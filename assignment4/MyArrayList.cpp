#include "MyArrayList.h"
#include <iostream>
#include <string>
using namespace std;

template <typename T>
MyArrayList<T>::MyArrayList()
{
   capacity = DEFAULT_CAPACITY;
   mass = new T[capacity];
}

template <typename T>
MyArrayList<T>::MyArrayList(int x)
{
   capacity = x;
   mass = new T[capacity];
}

template <typename T>
bool MyArrayList<T>::addElement(T const &x)
{
   if (size == capacity) {
      cout << "full" << endl;
      return false;
   }
   mass[size] = x;
   size++;
   return true;
}

template <typename T>
void MyArrayList<T>::printIt()
{
    cout << "{ "; 
    for(int i=0; i<size-1; i++) {
        cout << mass[i] << ", ";
    }
    cout << mass[size-1] << " }" << endl;
}