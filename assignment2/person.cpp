#include "person.h"
#include <string>
#include <iostream>

using namespace std;

person::person(){};
person::person(string profession, int age) {
    setProfession(profession);
    setAge(age);
};

void person::setProfession(string newProfession) {
    profession = newProfession;
}
void person::setAge(int newAge) {
    age = newAge;
}

string person::getProfession() {
    return profession;
}
int person::getAge() {
    return age;
}

void person::display() {
    cout << "My profession is: " << getProfession() << endl;
    cout << "My age is: " << getAge() << endl;
    walk();
    talk();
}
void person::walk() {
    cout << "I can walk" << endl;
}
void person::talk() {
    cout << "I can talk" << endl;
}




