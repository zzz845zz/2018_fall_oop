#include "footballer.h"
#include <iostream>
#include <string>

using namespace std;

footballer::footballer(string profession, int age) {
    setProfession(profession);
    setAge(age);
};

void footballer::playFootball() {
    cout << "I can play Football" << endl;
}