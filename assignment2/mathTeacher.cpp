#include "mathTeacher.h"
#include <iostream>
#include <string>

using namespace std;

mathTeacher::mathTeacher(string profession, int age) {
    setProfession(profession);
    setAge(age);
};

void mathTeacher::teachMath() {
    cout << "I can teach Maths" << endl;
};